package application;

import java.io.IOException;
import javax.swing.JOptionPane;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class GameScreenController {

    @FXML
    private Button button;

    @FXML
    private Button backToStart;

    @FXML
    private Label dalmatianLabel;

    @FXML
    private Label treatLabel;
    
	int dalmatians = 0;
	int dogTreats = 0;
	int clickMulti = 1;
	int treatMulti = 1;
	int treatBuffer = 0;
	UpgradeOneConstructor up = new UpgradeOneConstructor(0, 1);
    
    @FXML
    void handleButton(ActionEvent event) { System.out.println(up.getLevel() + " " + up.getClickMulti());
    	dalmatians = dalmatians + clickMulti;
    	treatBuffer = treatBuffer + clickMulti;
	    dalmatianLabel.setText("Dalmatians: " + Integer.toString(dalmatians));
	    if(treatBuffer - 101 > 0)
	    {
	    	dogTreats = dogTreats + treatMulti;
	    	treatLabel.setText("Treats: " + Integer.toString(dogTreats));
	    	treatBuffer = treatBuffer - 101;
	    }
	    switch(up.getLevel())
	    {
	    case 0:
	    	if(dalmatians >= 10)
	    	{
	    		JOptionPane.showMessageDialog(null, "Your parents got you your first puppy! \n (+1 Dalmatian per click)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(1);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti + up.getClickMulti();
	    	}
	    	break;
	    case 1:
	    	if(dalmatians >= 50)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Plantation unlocked. You have a lot of dogs, here is a place for them to stay! \n (+1 Dalmatian per click)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(2);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti + 1;
	    	}
	    	break;
	    case 2:
	    	if(dalmatians >= 101)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Station unlocked. Choo choo, all aboard! \n (+2 Dalmatians per click)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(3);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti + 2;
	    	}
	    	break;
	    case 3:
	    	if(dalmatians >= 250)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Sensation unlocked. Back in the 70's, Disco is all the Dalmatians knew. \n (+5 Dalmatians per click)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(4);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti + 5;
	    	}
	    	break;
	    case 4:
	    	if(dalmatians >= 750)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Tarnation unlocked. The rootin' tootin' dalmatian cowboys herd some more doggies! \n (+6 Dalmatians per click)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(5);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti + 6;
	    	}
	    	break;
	    case 5:
	    	if(dalmatians >= 1800)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Salvation unlocked. The Dalmatians have gone to church! \n (x2 Click multiplier increase)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(6);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti * 2;
	    	}
	    	break;
	    case 6:
	    	if(dalmatians >= 3500)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Vacation unlocked. The Dalmatians are enjoying a vacation scouting for more doggies! \n (+15 Dalmatians per click)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(7);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti + 15;
	    	}
	    	break;
	    case 7:
	    	if(dalmatians >= 7777)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Proclamation unlocked. The Dalmatian Population has formed a new nation! \n (x1.5 Click multiplier increase)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(8);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = (int) (clickMulti * 1.5);
	    	}
	    	break;
	    case 8:
	    	if(dogTreats >= 150)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Infestation unlocked. The Dalmatians have become so plentiful, they are everywhere! \n (x2 Click multiplier increase)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(9);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti * 2;
	    	}
	    	break;
	    case 9:
	    	if(dalmatians >= 30000)
	    	{
	    		JOptionPane.showMessageDialog(null, "Dalmatian Amalgamation unlocked. The Dalmatians are forming something stronger than we have ever seen! \n (x4 Click multiplier increase)", "Milestone Achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(10);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = (int) (clickMulti * 4);
	    	}
	    	break;
	    case 10:
	    	if(dalmatians >= 101000 & dogTreats >= 200)
	    	{	
	    		JOptionPane.showMessageDialog(null, "Dalmatian Domination unlocked. Dalmatians have taken over the Earth, unfathomable click increase. \n (You beat the game! x101 Click multiplier!)", "Milestone achieved!", JOptionPane.INFORMATION_MESSAGE);
	    		up.setLevel(11);
	    		up.setClickMulti(up.getLevel());
	    		clickMulti = clickMulti * 101;
	    	}
	    	break;
	    }
	    int randomEvent = (int) (Math.random() * 500); 
	    System.out.println(randomEvent);
	    switch(randomEvent)
	    {
	    case 20:
	    	JOptionPane.showMessageDialog(null, "You went to the Pet Store! +5 dog treats!", "Random Event", JOptionPane.INFORMATION_MESSAGE);
	    	dogTreats = dogTreats + 5;
	    	treatLabel.setText("Treats: " + Integer.toString(dogTreats));
	    	break;
	    case 60:
	    	JOptionPane.showMessageDialog(null, "Someone donated dog treats! +%5 of your current dog treats!", "Random Event", JOptionPane.INFORMATION_MESSAGE);
	    	dogTreats = (int) (dogTreats * 1.05);
	    	treatLabel.setText("Treats: " + Integer.toString(dogTreats));
	    	break;
	    case 157:
	    	JOptionPane.showMessageDialog(null, "Some of your dalmatians ran away. -%5 of your current dalmatians. :(", "Random Event", JOptionPane.INFORMATION_MESSAGE);
	    	dalmatians = (int) (dalmatians - (dalmatians * 0.05));
	    	dalmatianLabel.setText("Dalmatians: " + Integer.toString(dalmatians));
	    	break;
	    case 250:
	    	JOptionPane.showMessageDialog(null, "Your dalmatians are having lots of puppies! +%10 click multiplier!", "Random Event", JOptionPane.INFORMATION_MESSAGE);
	    	clickMulti = (int) (clickMulti * 1.1);
	    	break;
	    case 255:
	    	JOptionPane.showMessageDialog(null, "The pet store is having a sale on treats! +1 Dog Treat multiplier!", "Random Event", JOptionPane.INFORMATION_MESSAGE);
	    	treatMulti = treatMulti + 1;
	    	break;
	    case 331:
	    	JOptionPane.showMessageDialog(null, "Someone rescued a bunch of dalmatians! +%5 of your current dalmatians!", "Random Event", JOptionPane.INFORMATION_MESSAGE);
	    	dalmatians = (int) (dalmatians * 1.05);
	    	dalmatianLabel.setText("Dalmatians: " + Integer.toString(dalmatians));
	    	break;
	    case 404:
	    	JOptionPane.showMessageDialog(null, "One of your dogs found where you hide the treats. -%5 of your current dog treats. :(", "Random Event", JOptionPane.INFORMATION_MESSAGE);
	    	dogTreats = (int) (dogTreats - (dogTreats * 0.05));
	    	treatLabel.setText("Treats: " + Integer.toString(dogTreats));
	    	break;
	    case 420:
	    	JOptionPane.showMessageDialog(null, "A puppy found your treat stash. -5 treats", "Random Event", JOptionPane.INFORMATION_MESSAGE);
	    	dogTreats = dogTreats - 5;
	    	treatLabel.setText("Treats: " + Integer.toString(dogTreats));
	    	break;
	    }
    }
    @FXML
    void handleBackToStart(ActionEvent event) throws IOException{
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("Welcome.fxml"));
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }
}
