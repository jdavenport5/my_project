package application;

public class UpgradeOneConstructor {
	private int level;
	private int clickMulti;
	
	public UpgradeOneConstructor(int level, int clickMulti) {
		this.level = level;
		this.clickMulti = (clickMulti * level);
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getClickMulti() {
		return clickMulti;
	}

	public void setClickMulti(int clickMulti) {
		this.clickMulti = clickMulti;
	}
}
